﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examinationEsdp.Models
{
    public class ImageUrl
    {
        public int Id { get; set; }
        public int InstitutionId { get; set; }
        public string UserId { get; set; }
        public string FileUsl { get; set; }
    }
}
