﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace examinationEsdp.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public int ReviewId { get; set; }
        public int InstitutionId { get; set; }
        public int ImageId { get; set; }
    }
}
