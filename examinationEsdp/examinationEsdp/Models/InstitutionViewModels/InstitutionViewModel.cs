﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examinationEsdp.Models.InstitutionViewModels
{
    public class InstitutionViewModel
    {
        public List<ImageUrl> Image { get; set; }

        public Institution NameInstituon { get; set; }

        public List<Review> Rating { get; set; }
    }
}
