﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace examinationEsdp.Models.InstitutionViewModels
{
    public class CreateInstitutionViewModel
    {
        [Required(ErrorMessage = "Не указано название заведения")]
        [Display(Name = "Название заведения")]
        public string NameInstitution { get; set; }

        [Required(ErrorMessage = "Не выбрано фото заведения")]
        [Display(Name = "Фото заведения")]
        public IFormFile ImageUrl { get; set; }

        [Required(ErrorMessage = "Не указан тип заведения")]
        [Display(Name = "Тип заведения")]
        public string TypeOfInstitutionId { get; set; }

        [Required(ErrorMessage = "Не указано описание заведения")]
        [Display(Name = "Описание заведения")]
        public string Description { get; set; }
    }
}
