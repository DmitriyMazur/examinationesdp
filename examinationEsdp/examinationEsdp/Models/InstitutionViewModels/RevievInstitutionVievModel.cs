﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examinationEsdp.Models.InstitutionViewModels
{
    public class RevievInstitutionVievModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<ImageUrl> ImageUrl { get; set; }
        public List<Review> Review { get; set; }
        public int Rating { get; set; }
        public string ReviewMessage { get; set; }
    }
}
