﻿using System.Collections.Generic;
using examinationEsdp.Extensions;
using examinationEsdp.Models.ViewModel;

namespace examinationEsdp.Models.InstitutionViewModels
{
    public class InstitutionPagingViewModel
    {
        public PageViewModel PageViewModel { get; set; }
        public List<InstitutionViewModel> Institution { get; set; }
        public ForFilterViewModel FilterViewModel { get; set; }
    }
}
