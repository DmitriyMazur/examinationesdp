﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examinationEsdp.Models
{
    public class TypeOfInstitution
    {
        public int Id { get; set; }
        public string NameTypeOfInstitution { get; set; }
    }
}
