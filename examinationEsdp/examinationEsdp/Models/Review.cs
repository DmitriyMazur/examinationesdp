﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examinationEsdp.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string Rating { get; set; }
        public string UserId { get; set; }
        public string InstitutionId { get; set; }
    }
}
