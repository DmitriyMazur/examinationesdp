﻿using examinationEsdp.Data;
using examinationEsdp.Extensions;
using examinationEsdp.Models.InstitutionViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using examinationEsdp.Models.ViewModel;
using examinationEsdp.Services;

namespace examinationEsdp.Models
{
    public class InstitutionController : Controller
    {
        public ApplicationDbContext _context;
        public IHostingEnvironment _environment;
        public FileUploadService _fileUploadService;
        public PagingService _pagingService;


        public InstitutionController(ApplicationDbContext context, IHostingEnvironment environment, FileUploadService fileUploadService, PagingService pagingService)
        {
            _context = context;
            _environment = environment;
            _fileUploadService = fileUploadService;
            _pagingService = pagingService;
        }

        public IActionResult Index(string searchValue, int page = 1)
        {
            IEnumerable<Institution> institutions = _context.Institutions;

            List<InstitutionViewModel> institutionViewModels = new List<InstitutionViewModel>();

            
                institutions = ForSearchAndFiltr.FilterForEstablishment(searchValue, institutions.AsQueryable());

                foreach (var institution in institutions)
                {
                    institutionViewModels.Add(new InstitutionViewModel
                    {
                        NameInstituon = institution,
                        Image = _context.ImageUrls.Where(e => e.InstitutionId == institution.Id).ToList(),
                        Rating = _context.Reviews.Where(r => r.Id == institution.ReviewId).ToList()
                    });
                }
                
                PagedObject<InstitutionViewModel> pagedObject =
                    _pagingService.DoPage(institutionViewModels.AsQueryable(), page);

                InstitutionPagingViewModel model = new InstitutionPagingViewModel
                {
                    PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                    FilterViewModel = new ForFilterViewModel(searchValue),
                    Institution = pagedObject.Objects
                };
                return View(model);
            
        }

        public IActionResult RevievInstitution(int istitutionId)
        {
            Institution institution = _context.Institutions.FirstOrDefault(i => i.Id == istitutionId);
            List<ImageUrl> image = _context.ImageUrls.Where(i => i.InstitutionId == istitutionId).ToList();
            List<Review> reviews = _context.Reviews.Where(r => r.Id == institution.ReviewId).ToList();
            int rating = 0;
            foreach (var review in reviews)
            {
                rating = rating + Convert.ToInt32(review.Rating);
            }
            RevievInstitutionVievModel model = new RevievInstitutionVievModel
            {
                ImageUrl = image,
                Name = institution.Name,
                Description = institution.Description,
                Review = reviews,
                Rating = rating
            };
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.GroupModel = new SelectList(_context.TypeOfInstitutions, "Id", "NameTypeOfInstitution");
            return View();
        }

        [HttpPost]
        public IActionResult Create(string userName, CreateInstitutionViewModel model)
        {
            Institution institution = new Institution
            {
                Name = model.NameInstitution,
                TypeOfInstitutionId = Convert.ToInt32(model.TypeOfInstitutionId),
                Description = model.Description
            };
            _context.Institutions.Add(institution);
            _context.SaveChanges();

            ImageUrl imageUrl = new ImageUrl();
            if (model.ImageUrl != null)
            {
                imageUrl.FileUsl = Upload(institution.Id, model.ImageUrl, "images", "avatar", "supplier");
            }
            imageUrl.FileUsl = "images/DefaultLogos/DefaultGroupLogo.png";
            imageUrl.InstitutionId = _context.Institutions.FirstOrDefault(i => i.Name == model.NameInstitution).Id;
            imageUrl.UserId = _context.Users.FirstOrDefault(u => u.UserName == userName).Id;

            
            _context.ImageUrls.Add(imageUrl);
            _context.SaveChanges();
            return RedirectToAction("Index", "Institution");
        }

        [HttpPost]
        public void AddPost(RevievInstitutionVievModel model,string userName)
        {
            Institution institution = _context.Institutions.FirstOrDefault(i => i.Name == model.Name);
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.UserName == userName);
            Review review = new Review
            {
                Message = model.ReviewMessage,
                UserId = user.Id,
                InstitutionId = institution.Id.ToString(),
                Rating = model.Rating.ToString()
            };
            _context.Reviews.Add(review);
            _context.SaveChanges();
            RedirectToAction("RevievInstitution", "Institution", new { istitutionId = institution.Id});
        }

        public string Upload(int supplierId, IFormFile file, string folder, string secondFolder, string thirdFolder)
        {
            var path = Path.Combine(
                _environment.WebRootPath,
                $"{folder}\\{thirdFolder}\\{supplierId}\\{secondFolder}");
            _fileUploadService.Upload(path, file.FileName, file);

            return $"{folder}/{thirdFolder}/{supplierId}/{secondFolder}/{file.FileName}";
        }

        //private bool ValidateOnDefaultGroupLogo(IFormFile model = null, CreateInstitutionViewModel groupModel = null)
        //{
        //    if (groupModel != null && model == null)
        //    {
        //        groupModel.ImageUrl = $"images/DefaultLogos/DefaultGroupLogo.png";
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}
        
    }
}

