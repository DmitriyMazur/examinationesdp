﻿using examinationEsdp.Models;
using System.Linq;

namespace examinationEsdp.Extensions
{
    public class ForFilterViewModel
    {
        public string SearchValue { get; private set; }

        public ForFilterViewModel(string searchValue)
        {
            SearchValue = searchValue;
        }
    }
}
