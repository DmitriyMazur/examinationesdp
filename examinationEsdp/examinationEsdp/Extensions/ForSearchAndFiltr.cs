﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using examinationEsdp.Models;

namespace examinationEsdp.Extensions
{
    public class ForSearchAndFiltr
    {
        public static IQueryable<Institution> FilterForEstablishment(string searchValue, IQueryable<Institution> model)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                model = model.Where(m => m.Name.Contains(searchValue));
            }

            return model;
        }
    }
}
